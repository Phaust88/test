lyrics = [
    "Hello Darkness, my old friend",
    "I've come to talk with you again",
    "Because a vision softly creeping",
    "Left its seeds while I was sleeping",
    "And the vision that was planted in my brain",
    "Still remains",
    "Within the sound of silence"
]

centralizator = 50

title = "THE SOUND OF SILENCE"
form = "lyrics:"
rightPrettifier = ".~`*"
leftPrettifier = "*`~."

print(f"{' ' * (int((centralizator - len(title)) / 2) + len(rightPrettifier))}{title}\n\n{' ' * (int((centralizator - len(form)) / 2) + len(rightPrettifier))}{form}\n")

for line in lyrics:
    mult = int((centralizator - len(line)) / 2)
    space = " " * mult
    space2 = space
    if len(line) % 2 == 0:
        space2 = space[0:-1]
    print(f"{rightPrettifier}{space}{line}{space2}{leftPrettifier}")